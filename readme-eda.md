# rpi-backlight 编译

## 安装
1. 打包
   ```bash
   $ python3 setup.py bdist_wheel
   ```
2. 安装
   ```bash
   $ sudo pip3 install rpi_backlight-2.6.0-py3-none-any.whl
   ```
3. 卸载
   ```bash
   $ sudo pip3 uninstall rpi-backlight
   ```

## 桌面
install /home/pi/.local/share/applications/rpi-backlight.desktop
添加backlight文件操作权限规则
```bash
$ echo 'SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"' | sudo tee -a /etc/udev/rules.d/backlight-permissions.rules
```

## deb
python3 setup.py 制作deb
```bash
$ sudo apt-get install python3-stdeb fakeroot python3-all dh-python
$ python3 setup.py --command-packages=stdeb.command bdist_deb
```